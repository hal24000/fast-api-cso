from pydantic import BaseSettings


class CommonSettings(BaseSettings):
    APP_NAME: str = "CSO - FARM Stack"
    DEBUG_MODE: bool = False


class ServerSettings(BaseSettings):
    HOST: str = "0.0.0.0"
    PORT: int = 8000


class DatabaseSettings(BaseSettings):
    DB_URL: str = "mongodb+srv://cd_metwessexcso:lOPfNqR6WBJ0RApHwS82VyS0j2h10YcJ@atlas-prod-pl-0.hldmf.mongodb.net/cd_metwessexcso?retryWrites=true&w=majority"
    #"mongodb://applic:Ka9zSNti4hYu5RC3pd0m@10.0.1.4:27017,10.0.1.5:27017,10.0.1.6:27017/cd_metwessexcso?retryWrites=true&w=majority"
    DB_NAME: str = "cd_metwessexcso"


class Settings(CommonSettings, ServerSettings, DatabaseSettings):
    pass


settings = Settings()
