"""
App definition script, 'app' to be imported by all scripts that utilize it, e.g. scripts with callbacks.
"""
import dash
import math
import dash_bootstrap_components as dbc
import os
from flask_caching import Cache
from os import environ


#TODO: seems like this is unecessary. The stylesheets are loaded from the folder automatically anyway
# external_stylesheets = [dbc.themes.BOOTSTRAP,
#                         "assets/css/ywBootstrapTheme.css",
#                         "assets/css/custom.css",
#                         "assets/css/s1.css"]

app = dash.Dash(__name__)#, external_stylesheets=external_stylesheets)
app.config.suppress_callback_exceptions = True
app.config.update({
    # remove the default of '/'
    'routes_pathname_prefix': environ.get("URL_PREFIX"),
    'requests_pathname_prefix': environ.get("URL_PREFIX")
    #                            # remove the default of '/'
    # 'requests_pathname_prefix': ''
})
cache = Cache(app.server, config={
    'CACHE_TYPE': 'filesystem',
    'CACHE_DIR': 'cache-directory',
    'CACHE_DEFAULT_TIMEOUT': 0,
    'CACHE_THRESHOLD': math.inf
})
