from fastapi import APIRouter, Body, Request, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

from .models import UpdateCsoNotesModel

#from apps.app import app 

from config import settings

router = APIRouter()

@router.get("/debug", response_description="de bugging call")
async def debug_call(request: Request):
    """temp for debugging call
    """

    query_results = []
    #print("Getting Primary Keys")
    #app.mongodb_client[settings.DB_NAME]['cso_demo_asset_primary_key_table'].find({}, {'_id': False}).to_list(100) )
    #for doc in await app.mongodb_client[settings.DB_NAME]['cso_demo_asset_primary_key_table'].find({}, {'_id': False}).to_list(length=100): 
    #        #n ['cso_demo_asset_primary_key_table'].find({},)
    #        query_results.append(doc)
    query_results =  await app.mongodb['cso_demo_asset_primary_key_table'].find().to_list(1000)
    #await print("results" , query_results )

    return query_results


@router.get("/primary_keys", response_description="Primary keys of assets - used to check no duplicate ids")
async def get_primary_keys(request: Request):
    """Gives the primary key of assets - these are defined upon iongestion of data - see notebook 'database_writing' for method
    
    NTE: set to return only 100 values need to update this across app

    Args:

    None 

    Returns:
    list of dictionaries conatining following 

            {"primary_key": n,
            "node_id": "00000",
            "asset_type": "CSO"} # primary key built from node id and type
    
    """

    query_results = []
    print("Getting Primary Keys")
    #app.mongodb_client[settings.DB_NAME]['cso_demo_asset_primary_key_table'].find({}, {'_id': False}).to_list(100) )
    for doc in await request.app.mongodb_client[settings.DB_NAME]['cso_demo_asset_primary_key_table'].find({}, {'_id': False}).to_list(length=100): 
            #n ['cso_demo_asset_primary_key_table'].find({},)
            query_results.append(doc)
    #query_results =  await app.mongodb['cso_demo_asset_primary_key_table'].find().to_list(1000)
    #await print("results" , query_results )

    return query_results


@router.get("/asset_meta_data", 
            response_description="Gives Node/Asset meta data - node id, location, type, install date", 
            )
async def get_asset_meta_data(request: Request):
    """Gives Node/Asset meta data - node id, location, type, install date
    
    Agrs 
    None 

    Retruns 
    List of dictionary with following 

                {
                "node_id": "14002",
                "type": "CSO",
                "primary_key": 1,
                "installation_date": "unkown",
                "site_name": "Bath Twerton"
            }, # build up to include more relavant data

    NOTE same issue with list as above
                
    """
    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_asset_meta'].find({}, {'_id': False}).to_list(length=100): #n ['cso_demo_asset_primary_key_table'].find({},)
        query_results.append(doc)
    return query_results

@router.get("/asset_position_data", response_description="Primary keys of assets - used to check no duplicate ids")
async def get_asset_network_position_data(request: Request):
    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_asset_location'].find({}, {'_id': False}).to_list(length=100): #n ['cso_demo_asset_primary_key_table'].find({},)
        query_results.append(doc)
    return query_results

@router.get("/cso_data", response_description="Primary keys of assets - used to check no duplicate ids") # NOT A STATCI TABLE 
async def get_cso_data(request: Request):
    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_cso'].find({}, {'_id': False}).to_list(length=100): #n ['cso_demo_asset_primary_key_table'].find({},)
        query_results.append(doc)
    return query_results

 
@router.post("/addnote_node_id", response_description="Add new note")
async def set_user_note(request: Request,  node_note: UpdateCsoNotesModel = Body(...)): #node_id : str, for later use - updating by user 
    node_note = jsonable_encoder(node_note)
    new_node_note = await request.app.mongodb["cso_demo_user_notes"].insert_one(node_note)
    created_node_note = await request.app.mongodb["cso_demo_user_notes"].find_one({}, 
                                                                        {"_id": str(new_node_note.inserted_id)})


    return JSONResponse(status_code=status.HTTP_201_CREATED, content=str(created_node_note))

@router.get("/sewer_node_notes", response_description="view notes on nodes in system")
async def get_node_notes(request: Request):
    query_results = []

    for doc in await request.app.mongodb["cso_demo_user_notes"].find({}, {'_id': False}).to_list(length=100):
        query_results.append(doc)
    return query_results

@router.get("/sewer_network_ids", response_description="list of sewer clusters in network")
async def list_networks(request: Request):
    query_results = []
    for doc in await request.app.mongodb[ 'test_network_locations'].aggregate([ #'cso_demo_table_asset_location'
                                                                            {'$group': {
                                                                                '_id': 'null',
                                                                                'network_cluster_id': {'$addToSet': '$network_cluster_id'},
                                                                                'network_cluster_name': {'$addToSet': '$network_cluster_name'},
                                                                                }}
                                                                            ]).to_list(length=100): #n ['cso_demo_asset_primary_key_table'].find({},)
                                                                            query_results.append(doc)
    return query_results


