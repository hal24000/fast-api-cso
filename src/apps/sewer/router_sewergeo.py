from fastapi import APIRouter, Body, Request, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder


router = APIRouter()



@router.get("/network_{cluster_id}_combined_sewer_overflow_geojson.geojson", response_description="Get a single network geojson")
async def get_combined_sewer_overflow_geo_data(cluster_id: str, request: Request):


    if (query_result := await request.app.mongodb['cso_demo_geojsons'].find_one({ 'filename' : f'cluster_{cluster_id}_type_CSO.geojson' }, {"_id": False})) is not None:
        return query_result

    raise HTTPException(status_code=404, detail=f"No geojson found for network clsuter {cluster_id}")


@router.get("/network_{cluster_id}_pumpingstation_geojson", response_description="Get a single network pumping station geojson")
async def get_pumping_stations_geo_data(cluster_id: str, request: Request):


    if (query_result := await request.app.mongodb['cso_demo_geojsons'].find_one({ 'filename' : f'cluster_{cluster_id}_type_PumpingStation.geojson' }, {"_id": False})) is not None:
        return query_result

    raise HTTPException(status_code=404, detail=f"No geojson found for network clsuter {cluster_id}")


@router.get("/network_{cluster_id}_water_recycling_center_geojson", response_description="Get a single network pumping station geojson")
async def get_water_recycling_center_geo_data(cluster_id: str, request: Request):

    if (query_result := await request.app.mongodb['cso_demo_geojsons'].find_one({ 'filename' : f'cluster_{cluster_id}_type_WaterRecyclingCentre.geojson' }, {"_id": False})) is not None:
        return query_result

    raise HTTPException(status_code=404, detail=f"No geojson found for network clsuter {cluster_id}")

