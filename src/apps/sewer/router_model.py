

import json


import pandas as pd 
import numpy as np


from fastapi import APIRouter, Body, Request, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

from datetime import datetime 
from typing import Optional




import prophet
from prophet import Prophet
from prophet.serialize import model_to_json, model_from_json


#print("VERSION Prophet", prophet.__version__)


router = APIRouter()

##### HELPERS #########

async def helper_get_local_target_data(request, target):
    target_local_data = await request.app.mongodb['cso_demo_table_asset_location'].find({ 'node_id' : { '$eq': target } },
                                        {"_id": False},
                                    ).to_list(100)
    print("TAGERT DATA", target_local_data)
    return target_local_data

async def helper_get_pretrained_model(request, target, model = "prophet_forecast"):
    print(f"Getting Target {target} model {model}")
    get_stored_model = await request.app.mongodb['WESSEX_Models'].find_one( { '$and': [ { 'sensor_id' : target},
                                                                                        {'model_name' : model}, 
                                                                                        ]},
                                                                                        {"_id": False},
                                                                                )

    print(f"GOT STORED MODEL {target} model {model}")
    
    #deserailised_model = model_from_json(json.dumps(get_stored_model))

    get_stored_model = json.dumps(get_stored_model)

    get_stored_model = model_from_json(get_stored_model)

    

    return get_stored_model.predict().to_json() # get_stored_model # deserailised_model # get_stored_model # 

def helper_get_pretrained_model_test(request, target, model = "prophet_forecast"):
    print(f"Getting Target {target} model {model}")
    get_stored_model = request.app.mongodb['WESSEX_Models'].find_one( { '$and': [ { 'sensor_id' : target},
                                                                                        {'model_name' : model}, 
                                                                                        ]},
                                                                                        {"_id": False},
                                                                                )

    print(f"GOT STORED MODEL {target} model {model}")
    
    deserailised_model = model_from_json(json.dumps(get_stored_model))

    return deserailised_model # get_stored_model # 



async def helper_get_sensor_raindata(request, target):
    network_data = await helper_get_local_target_data(request = request, target=target)
    print("NETWORK DATA", network_data)
    cluster_id = network_data[0]['network_cluster_id']
    print("CLUSTER ID: ", cluster_id)

    rain_data = await request.app.mongodb['cso_demo_rainfall'].find({ },
                                        {
                                        f"rain_shift_neg1_cluster{cluster_id}_loc01" : True, 
                                        "Datetime" : True, 
                                        "_id" : False, 
                                        },
                                    ).to_list(1000)
   

    
    print("RAIN DATA ")
    return  rain_data

async def helper_get_target_custer_data(request, target):
    target_local_data = await helper_get_local_target_data(request = request, target=target)

    # add errors 
    if len(target_local_data) == 1:
        print("target data has single match")
        downstream_nodes = target_local_data[0]['downstream_node_ids']
        print("DOWNSTREAM NODES", downstream_nodes)
        upstream_nodes = target_local_data[0]['upstream_node_ids']
        print("UPSTREAM NODES", upstream_nodes)
    elif len(target_local_data) > 1:
        print("duplicate records for target data found, inspect mongo collection")
    else:
        print("No record found")

    print(target_local_data)

    # combine all ids 
    node_ids = []
    node_ids.append(target)


    def append_node_ids_to_list(list_, append_):
        """Appends node id or list of node ids to list_"""
        if type(append_) == list:
            node_ids.extend(append_ )
        elif type(append_) == str:
            node_ids.append(append_)
        else: 
            raise TypeError(f"tyre should be a list or string not {type(append_)}")
        return node_ids

    node_ids =  append_node_ids_to_list(list_ = node_ids, append_ = upstream_nodes)
    node_ids =  append_node_ids_to_list(list_ = node_ids, append_ = downstream_nodes)

    print("NODE IDs: ", node_ids)


    
    return node_ids # targ
    


##### API defintions #########


@router.get("/forecast_{model}_target_{target}", response_description="Primary keys of assets - used to check no duplicate ids")
async def get_pretrainned_model(request: Request, target : str, model : Optional[str] = 'prophet_forecast'):
    print("Getting model")
    response = await helper_get_pretrained_model(request = request, target = target, model = model) 
    #deserailised_model = model_from_json(json.dumps(stored_model))
    #df_prediction = deserailised_model.predict()
    #response = df_prediction.to_json()
    print("STORED MODEL ", response)
    return response # deserailised_model #


#@router.get("/prophet_model_target_{target}_start_date_{start_date}_days{days}", response_description="Primary keys of assets - used to check no duplicate ids")
@router.get("/target_network_cluster_data_{target}", response_description="list of nodes connected to and including the target - both upstream and downstream")
async def get_network_cluster_data(request: Request, target : str): # start_date : str = '2019-07-08', days : int = 21
    """Gets a lists of nodes that are directly upstream and downstream of target node, also conatins target node
    
    Agrs:

    string of target id 

    Returns:

    list of nodes in local cluster

    In Use for:
        Getting data of surrounding nodes


    """
    
    print("TARGET", target)
    target_custer_data = await helper_get_target_custer_data(request = request, target = target)
    return  target_custer_data


@router.get("/sensor_rain_data_{target}", response_description="Primary keys of assets - used to check no duplicate ids")
async def get_rain_data(request: Request, target : str):

    rain_data = await helper_get_sensor_raindata(request, target)

    return rain_data # get_stored_model # deserailised_model #

@router.get("/prophet_results_df{target}_{start_date}", 
            response_description="Primary keys of assets - used to check no duplicate ids")
async def get_model_prediction(request: Request, target : str, start_date: str ): #startdate: str, 

    # start_date = '2019-04-01 00:00:00' 
    
    

    end_date = str(pd.to_datetime(start_date) + pd.DateOffset(days=2))
    query = { 'Datetime': { '$gt' : start_date, 
                            '$lt' : end_date ,
                            }, 
                            }

    node_ids = await helper_get_target_custer_data(request, target)

    subset  =  {**{'Datetime': 1} , **{ i : 1 for i in node_ids}, **{'_id' : 0 }}

    sensor_data = await request.app.mongodb['cso_demo_table_cso_feed'].find( query,
                                                                subset,
                                                                ).to_list(100)
   

    rain_data = await helper_get_sensor_raindata(request, target)
   

    dfsensors = pd.DataFrame(sensor_data)
   
    
    dfsensors.set_index('Datetime', inplace=True)
    dfsensors.index =   pd.DatetimeIndex(dfsensors.index)
    dfsensors =  dfsensors.resample('H').mean()

    

    
    dfrain =  pd.DataFrame( rain_data)
    dfrain.rename(columns = {'rain_shift_neg1_cluster01_loc01': "rain_shift-1" }, inplace =True)
    dfrain.set_index('Datetime', inplace=True)

    # get the dataframe and convert to 60 min mean for model

    #df_model_input = pd.merge(dfsensors, dfrain, how = 'left', left_on= 'Datetime', right_on= 'Datetime')
    #df_model_input = pd.concat([dfsensors, dfrain], axis=1 )
    print("DTYPE", dfsensors.index.dtype ) # 15 mins
    print("DTYPE", dfrain.index.dtype )  # hourly 


    #df_model_input = dfsensors.merge(dfrain, left_index=True, right_index=True)
    df_model_input = pd.concat([dfsensors, dfrain], axis=1 )
    print("HEADERS", list(df_model_input) )
    #df_model_input.rename({'left_Datetime': 'Datetime'}, axis = 1, inplace= True)
    #df_model_input.index =   pd.DatetimeIndex(df_model_input.index)
    
    deserailised_model = await helper_get_pretrained_model(request = request, target = target) 
    
    df_prediction = df_model_input.reset_index().copy()
    df_prediction  = df_prediction.dropna(axis = 0, how = 'any')
    print("DF PED", list(df_prediction))
    df_prediction.rename(columns={"Datetime": "ds", target: "y"},  inplace =True)
    df_prediction["yhat"] = deserailised_model.predict(df_prediction.drop(["y"], axis=1))["yhat"]
    df_prediction["yhat_lower"] = deserailised_model.predict(df_prediction.drop(["y"], axis=1))["yhat_lower"]
    df_prediction["yhat_upper"] = deserailised_model.predict(df_prediction.drop(["y"], axis=1))["yhat_upper"]
    endpointdata = df_prediction.to_json(orient = 'columns' ) 
    return endpointdata

