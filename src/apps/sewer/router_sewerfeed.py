from fastapi import APIRouter, Body, Request, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from datetime import date, datetime
import pandas as pd
import urllib.request


router = APIRouter()


async def helper_get_local_cluster_nodes( request, target = '14002'):
    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_asset_location'].find( { "node_id" : { '$eq': target }}, 
                                                        {'_id': False, }).to_list(length=1000): #n ['cso_demo_asset_primary_key_table'].find({},)
        query_results.append(doc)
    print("HELPER QUERY, ", query_results)
    # add check to ensure list is only 1 long ie index 0 is all there is 
    if query_results[0]['upstream_node_ids'] != 'No Upstream':
        upstream = query_results[0]['upstream_node_ids']
    else:
        upstream = None

    if query_results[0]['downstream_node_ids'] != 'No Downstream':
        downstream = query_results[0]['upstream_node_ids']
    else:
        downstream = None
    

    downstream = query_results[0]['downstream_node_ids']
    print("upstream ", upstream, " downstream" )

    if downstream:
        nodes_in_cluster = [target, downstream] # note will fail if downstream is list 
    else:
        nodes_in_cluster = [target]

    if upstream:
        nodes_in_cluster.extend(upstream)
    else: 
        pass
    print("nodes in clusterm ", upstream)

    return nodes_in_cluster
        


@router.get("/cso_level_feed", response_description="Primary keys of assets - used to check no duplicate ids")
async def get_cso_level_feed(request: Request):
    """gets the cso level by time - currently set to return a list of length 1000 - needs extending out"""
    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_cso_feed'].find({}, {'_id': False}).to_list(1000): # change to date 
        query_results.append(doc)
    return query_results

@router.get("/cso_level_feed_start_{start_date}_end_{end_date}", response_description="Primary keys of assets - used to check no duplicate ids")
async def get_cso_level_feed(request: Request, start_date : str, end_date : str ):
    """gets the cso level by time - currently set to return a list of length 100 - needs extending out"""

    # get dates in format 
    # dates come from dash
    # length of list deterermined by dates start to end = days * (4 * 24) 
    # can we do it another way? 
    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_cso_feed'].find({}, {'_id': False}).to_list(1000): # change to date 
        query_results.append(doc)
    return query_results


@router.get("/cso_level_feed_by_parent_{parent_node}_start_{start_date}_end_{end_date}", 
            response_description="Levels of local cluster, defined by parent node and dates")
async def get_cso_level_feed_by_parent_node(request: Request, parent_node : str, start_date : str, end_date : str ):
    """gets the cso level by time for local cluster of a parent cluster
    - currently set to return a list of length 100 - needs extending out"""

    # get dates in format 
    # dates come from dash
    # length of list deterermined by dates start to end = days * (4 * 24) 
    # can we do it another way? 
    node_list = await helper_get_local_cluster_nodes( request = request,  target = parent_node)
    print("NODE LIST : ", node_list)
    #node_list = ["14002"]
    subset = {i: 1 for i in node_list }
    add_defaults = {"_id": 0, "Datetime" : 1 }
    subset_all = {**subset, **add_defaults}

    start = datetime.strptime(start_date, '%d-%M-%Y')
    end = datetime.strptime(end_date, '%d-%M-%Y')


    print("END DATE FROM TO : ", end_date, end)
    print("START DATE FROM TO : ", start_date, start)


    query = {"Datetime" : { '$lt' : end, 
                            '$gte': start, 
                             } }
    days = (end-start).days
    list_length = days * 24 * 4
    print("Total Data points", list_length)

    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_cso_feed'].find( query, subset_all
                                                                 ).to_list(1000): # change to date  list_length
        query_results.append(doc)

    #items = [x async for x in request.app.mongodb['cso_demo_table_cso_feed'].find( query, subset_all)]

    return query_results

@router.get("/pumping_stations_on_off_feed", response_description="Primary keys of assets - used to check no duplicate ids")
async def get_pumping_station_on_off_time(request: Request):
    """Gets the on off times of a pumping station - to do add option to select pumping station"""
    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_pumping_stations_feed'].find({}, {'_id': False}).to_list(1000): # change to date ""
        query_results.append(doc)
    return query_results

@router.get("/water_recycling_center_level_feed", response_description="Primary keys of assets - used to check no duplicate ids")
async def get_WRC_level_feed(request: Request, cluster_id: str = 0o01, ):
    """provides timeseries for Water Recycling Center - to do add cluster refeece in DB"""
    query_results = []
    for doc in await request.app.mongodb['cso_demo_table_water_recycling_center_feed'].find({}, {'_id': False}).to_list(1000): # change to date
        query_results.append(doc)
    return query_results



# do below 
@router.get("/network_{cluster_id}_cso_model", response_description="Get a single network pumping station geojson")
async def get_pumping_stations(cluster_id: str, request: Request):
    """Gets the geojson for a nodes that are pumping stations"""

    if (query := await request.app.mongodb['cso_demo_geojsons'].find_one({ 'filename' : f'cluster_{cluster_id}_type_PumpingStation.geojson' }, {"_id": False})) is not None:
        return query

    raise HTTPException(status_code=404, detail=f"No geojson found for network clsuter {cluster_id}")



'cso_demo_table_cso'