@router.get("/network_model_test2", response_description="Get model result for a given station")
def get_prediction_test3( target ):


    class ProjectCollections(dimension.Connect):
        
        """
        Use this class to aceess a Mongo DB instance and use the data, 
        requires a path to the config file and connects through dimension conect 
        other methods are native to pymongo see here https://pymongo.readthedocs.io/en/stable/index.html
        """
        
        def __init__(self, path : str):
            """Initalise connection requires path to config file"""
            self.dc = dimension.Connect(path)
            self.db = self.dc.db()
            super().__init__(path)
            
        def get_collections(self, searchstring : str = "") -> list:
            """Lists collections in the database, use searchstring to find sepcific collections"""
            relevent_collections = [col for col in self.dc.collections() if searchstring in col]
            return relevent_collections
            
        def get_data_obj(self, collection : str, query: dict = {}, subset : dict = {"_id": 0} ) -> dimension.Connect:
            """given a collection name gets collection object"""
            data_collection_obj  =  self.db[collection].find(query, subset)
            return data_collection_obj 
        
        def get_data(self, collection : str, query: dict = {}, subset : dict = {"_id": 0} ) -> list:
            """given a collection name gets data as list from collection object, for use in API"""
            data_collection_list = list(self.get_data_obj(collection, query,  subset))
            return data_collection_list

    ####



    FILEPATH = 'src/config/mongo_conf_ww.txt'
    mongo_conn = ProjectCollections(path = FILEPATH)


    #target = '16017' 
    node_id = target 


    #node_ids =  ['16017', '16016', '17589', '19704', '14002']

    downstream_node_ids = list(mongo_conn.db.cso_demo_table_asset_location.find( { 'node_id' : { '$eq': node_id } } ,
                                                                 {'downstream_node_ids'}))[0][ 'downstream_node_ids']

    print("DOWN STREAM NODES: .", downstream_node_ids)

    upstream_node_ids = list(mongo_conn.db.cso_demo_table_asset_location.find( { 'node_id' : { '$eq': node_id } } ,
                                                                 {'upstream_node_ids'}))[0][ 'upstream_node_ids']

    print("DOWN STREAM NODES: .", upstream_node_ids)


    # combine all ids 
    node_ids = []
    node_ids.append(target)

    def append_node_ids_to_list(list_, append_):
        """Appends node id or list of node ids to list_"""
        if type(append_) == list:
            node_ids.extend(append_ )
        elif type(append_) == str:
            node_ids.append(append_)
        else: 
            raise TypeError(f"tyre should be a list or string not {type(append_)}")
        return node_ids

    node_ids =  append_node_ids_to_list(list_ = node_ids, append_ = upstream_node_ids)
    node_ids =  append_node_ids_to_list(list_ = node_ids, append_ = downstream_node_ids)

    print("NODE IDs: ", node_ids)



    print(f"GETTING STORED MODEL FOR: {node_id}")
    get_stored_model =  mongo_conn.db['WESSEX_Models'].find_one({ 'sensor_id' : target, 
                                                                            "model_name": "prophet", 
                                                                            "model_start_year": "2019"},
                                        {"_id": False},
                                    )

    


                                    

    deserailised_model = model_from_json(json.dumps(get_stored_model))

    start_date = datetime.strptime("2019/06/10", "%Y/%m/%d")
    #upstream = '16016' 
    #downstream = '14002'
    cso_cluster = node_ids 

    #db = mongo_conn.db

    test_days = 21
    real_days = 14
    test_start_date = pd.date_range(start_date, periods=24 * test_days, freq="H")[0]
    real_end_date = pd.date_range(start_date, periods=24 * real_days, freq="H")[-1]
    test_end_date = pd.date_range(start_date, periods=24 * test_days, freq="H")[-1]

    query = {"Datetime": {"$gte": test_start_date, "$lte": test_end_date}}
    project = {"_id": 0, "Datetime": 1, "rain_shift-1": 1}
    project.update({cso: 1 for cso in cso_cluster})

    
    


    # model here bad need to 1 call model 2 
    dfdata = list(mongo_conn.db["WESSEX_E_Numbers_Apr_2019_60Min_Processed"].find(query, project))
    print("df data", dfdata)

    tesdf = pd.DataFrame(data = [[0,1],  [1, 0]], columns = [["foo", "bar"]])
    
    print("TEST DF ", tesdf )

    df = (
        pd.DataFrame(
            dfdata
        )
        .set_index("Datetime")
        .sort_index()
    )
    res_df = df.reset_index().rename(columns={"Datetime": "ds", target: "y"})
    res_df

    res_df["yhat"] = deserailised_model.predict(res_df.drop(["y"], axis=1))["yhat"]
    res_df["yhat_lower"] = deserailised_model.predict(res_df.drop(["y"], axis=1))["yhat_lower"]
    res_df["yhat_upper"] = deserailised_model.predict(res_df.drop(["y"], axis=1))["yhat_upper"]
    res_df["alarm"] = np.where(
            ((res_df.y < res_df.yhat_lower) | (res_df.y > res_df.yhat_upper)),
            1,
            0,
        )

    res_df = res_df.set_index("ds")
    #alarms_list = res_df[test_start_date:real_end_date][res_df[test_start_date:real_end_date]["alarm"] == 1].index
    
    endpointdata = res_df.reset_index().to_json(orient = 'columns' ) # {‘split’, ‘records’, ‘index’, ‘columns’, ‘values’, ‘table’}
    
    return endpointdata