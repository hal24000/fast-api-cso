
import uuid

import datetime as datetime

from typing import Optional

from pydantic import BaseModel, Field


class TaskModel(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    name: str = Field(...)
    completed: bool = False

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "id": "00010203-0405-0607-0809-0a0b0c0d0e0f",
                "name": "My important task",
                "completed": True,
            }
        }


class UpdateCsoNotesModel(BaseModel):
    node_id: str = Field(...)
    user_notes: str = Field(...)
    time_added: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "node_id" : '12345',
                "user_notes": "Add a new note and updates data base",
                "time_added": datetime.datetime.now(),
            }
        }
