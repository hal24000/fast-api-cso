from fastapi import APIRouter, Body, Request, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

from .models import TaskModel, UpdateTaskModel

router = APIRouter()


@router.get("/primary_keys", response_description="Primary keys of assets - used to check no duplicate ids")
async def list_primary_keys(request: Request):
    tasks = []
    for doc in await request.app.mongodb['cso_demo_asset_primary_key_table'].find({}, {'_id': False}).to_list(length=100): #n ['cso_demo_asset_primary_key_table'].find({},)
        tasks.append(doc)
    return tasks


@router.get("/network_{cluster_id}_cso_geojson", response_description="Get a single network geojson")
async def show_task(cluster_id: str, request: Request):


    if (task := await request.app.mongodb['cso_demo_geojsons'].find_one({ 'filename' : f'cluster_{cluster_id}_type_CSO.geojson' }, {"_id": False})) is not None:
        return task

    raise HTTPException(status_code=404, detail=f"No geojson found for network clsuter {cluster_id}")


@router.get("/network_{cluster_id}_pumpingstation_geojson", response_description="Get a single network pumping station geojson")
async def show_task(cluster_id: str, request: Request):


    if (task := await request.app.mongodb['cso_demo_geojsons'].find_one({ 'filename' : f'cluster_{cluster_id}_type_PumpingStation.geojson' }, {"_id": False})) is not None:
        return task

    raise HTTPException(status_code=404, detail=f"No geojson found for network clsuter {cluster_id}")


@router.get("/network_{cluster_id}_water_recycling_center_geojson", response_description="Get a single network pumping station geojson")
async def show_task(cluster_id: str, request: Request):

    if (task := await request.app.mongodb['cso_demo_geojsons'].find_one({ 'filename' : f'cluster_{cluster_id}_type_WaterRecyclingCentre.geojson' }, {"_id": False})) is not None:
        return task

    raise HTTPException(status_code=404, detail=f"No geojson found for network clsuter {cluster_id}")

