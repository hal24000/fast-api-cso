
from fastapi import FastAPI


tags_metadata = [
    
    {
        "name": "sewer_meta_data",
        "description": "Get the meta data for nodes",
        "externalDocs": {
            "description": "Further Information",
            "url": "https://www.hal24k-water.com/collaborate-ecosystem",
        },
    },

    {
        "name": "sewer_meta_data",
        "description": "Get the meta data for nodes",
        "externalDocs": {
            "description": "Further Information",
            "url": "https://www.hal24k-water.com/collaborate-ecosystem",
        },
    },

    {
        "name": "sewer_feed_data",
        "description": "Get telemetry data for nodes",
        "externalDocs": {
            "description": "Further Information",
            "url": "https://www.hal24k-water.com/collaborate-ecosystem",
        },
    },


    {
        "name": "sewer_geojson_data",
        "description": "Get geojson data for nodes",
        "externalDocs": {
            "description": "Further Information",
            "url": "https://www.hal24k-water.com/collaborate-ecosystem",
        },
    },

     {
        "name": "sewer_models",
        "description": "Train and Get Models",
        "externalDocs": {
            "description": "Further Information",
            "url": "https://www.hal24k-water.com/collaborate-ecosystem",
        },
    },

    





    
    {
        "name": "test",
        "description": "for testing new APIS",

    },
]

description = """

<img src="static\collaborate-dimension.png" alt="drawing" style="border:red"/>

HAL24K Water's Collaborate Data Store Sewer API helps you

## Get Data Easily - Work effortlessly with APIs - Enrich Data 

**Get Data** from the API and use it anywhere from PowerBI and Excel to fully custom dashboards. **Working with APIs is easier than ever before!** 

**No developer needed!** Easily Investigate APIs With our API user interface **Enrich your data** with other sources and allow stakeholders to add notes to every node in your network! 

## More Info 
"""



app = FastAPI(  docs_url = '/collaborate_api_docs',
                title="Collaborate Sewer API - Get Value from Waste Data ",
                description= description,
                version="0.0.1",
                # below should render but not working for some reason evene with example given on fastapi
                # https://fastapi.tiangolo.com/tutorial/metadata/
                terms_of_service="https://www.hal24k-water.com/collaborate-ecosystem",
                contact={
                    "name": "Company info & Contant",
                    "url": "https://www.hal24k-water.com",
                    #"email": "dp@x-force.example.com",
                },
                license_info={
                    "name": "Private License",
                    "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
                },
                
                docExpansion='None', # will/should work in next release of fastapi 
                openapi_tags=tags_metadata,
                ) # APP Instance 
