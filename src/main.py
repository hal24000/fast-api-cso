# copyright HAL24K-WATER 2021 
# authors @Scott.Charlesworth 


import uvicorn

from motor.motor_asyncio import AsyncIOMotorClient
from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles

from config import settings
from apps.app import app 
from apps.sewer.router_sewerstatic import router as sewermeta
from apps.sewer.router_sewergeo import router as sewergeo
from apps.sewer.router_sewerfeed import router as sewerfeed
from apps.sewer.router_model import router as models

import os 
print("WORKING DIRCETORY, ", os.getcwd())

#################### DB CONNECTIONS #############################

@app.on_event("startup")
async def startup_db_client():
    """Connects APP to Mongo DB via Motor"""
    #print("URL ", settings.DB_URL)
    print("Connecting")
    app.mongodb_client = AsyncIOMotorClient(settings.DB_URL)
    app.mongodb = app.mongodb_client[settings.DB_NAME]
    print("SERVER INFO", await app.mongodb_client.server_info())
    print("app connection", app.mongodb)
    print("test collection, ", await app.mongodb['cso_demo_asset_primary_key_table'].find({}, {'_id': False}).to_list(100) )
    


@app.on_event("shutdown")
async def shutdown_db_client():
    """Closes connection to client  when app shuts down"""
    app.mongodb_client.close()

#################### END DB CONNECTIONS #############################


#################### STATIC PAGES #############################

#@app.get("/")
#def serve_home(request: Request):
#    return "<H1> FOO BAR </H1>" #templates.TemplateResponse("index.html", context= {"request": request})

#app.mount("/static", StaticFiles(directory="apps/static", html = True), name="static")

app.mount("/src/static", StaticFiles(directory="/src/static"), name="static")

#################### END STATIC PAGES #############################

#################### API PAGES #####################################



app.include_router(sewermeta, tags=["sewer_meta_data",], prefix="/sewer_static_data")
app.include_router(sewergeo, tags=["sewer_geojson_data",], prefix="/sewer_geo_data")
app.include_router(sewerfeed, tags=["sewer_feed_data",], prefix="/sewer_feed_data")
#app.include_router(overflowmodel, tags=["sewer_overflow_model",], prefix="/sewer_overflow_model")
app.include_router(models, tags=["sewer_models",], prefix="/sewer_models")
#app.mount("apps/static", StaticFiles(directory="static"), name="static")
#################### END API PAGES #####################################


################ RUN APP via UVICORN ###################################

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=settings.HOST,
        reload=settings.DEBUG_MODE,
        port=settings.PORT,
    )
